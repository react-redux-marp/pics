import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 6cf68d74b0981f8a04f311a91f50d20acbdadcd10d85f574d7e8119a1d54bae8'
    }
});