import './image-list.css';
import React from 'react';
import ImageCard from './image-card';

class ImageList extends React.Component {

    buildImages = (images) => {
        let count = 1, response = {images4_1:[], images4_2:[], images4_3: [], images4_4: []};
        for(let img of images){
            if(count % 4 == 0){
                response.images4_4.push(
                    <ImageCard key={img.id} image={img}/>
                );
            }else if(count % 3 == 0){
                response.images4_3.push(
                    <ImageCard key={img.id} image={img}/>
                );
            }else if(count % 2 == 0){
                response.images4_2.push(
                    <ImageCard key={img.id} image={img}/>
                );
            }else{
                response.images4_1.push(
                    <ImageCard key={img.id} image={img}/>
                );
            }
            count += 1;            
        }
        return response;
    }

    render(){
        //awful solution. do not try
        //const { images4_1, images4_2, images4_3, images4_4 } = this.buildImages(this.props.images);

        const images = this.props.images.map((img) => {
            return <ImageCard
                    key={img.id}
                    image={img}
                    className="card"/>;
        });

        return <div className={`card-columns image-list wrapper ${this.props.className}`}>
            {images}
        </div>;
    }
}

export default ImageList;