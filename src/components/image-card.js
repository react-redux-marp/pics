import React from 'react';

class ImageCard extends React.Component{
    constructor(props){
        super(props);
        this.imageRef = React.createRef();
    }
    render(){

        const {links, description, urls, alt_descrition, user} = this.props.image;

        return <a href={links.html}
        title={description}
        className={`image-list-item image-card ${this.props.className}`}>
        <img
            ref={this.imageRef}
            src={urls.regular}
            alt={alt_descrition + " from: " + user.username}
            className="w-100"/>
        </a>;
;
    }
}

export default ImageCard;