import React from 'react';
import axios from '../api/unsplash';
import SearchBar from './search-bar';
import ImageList from './image-list';

class App extends React.Component{

    state = { images: [] }

    onSearchSubmit = async (state, ev) => {
        const response = await axios.get('/search/photos', {
            params: { query: state.term },
        });
        this.setState({images:response.data.results});
        console.log(this.state.images);
    }
    onMoreImages = (ev) => {

    }
    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <SearchBar className="mt-3" onSubmit={this.onSearchSubmit}/>
                    </div>
                </div>
                <ImageList images={this.state.images} onRequestImages={this.onMoreImages}/>
            </div>
        );
    }
}

export default App;