import React from 'react';

class SearchBar extends React.Component {

    state = {term: ''}

    onInputChange = (ev) => {
        this.setState({term: ev.target.value});
    }
    onFormSubmit = (ev) => {
        ev.preventDefault();
        if(this.props.onSubmit){
            this.props.onSubmit(this.state, ev);
        }
    }
    render(){
        return (
            <div className={`${this.props.className} ui segment`}>
                <form onSubmit={this.onFormSubmit} className="ui form search-form">
                    <div className="field">
                        <label>Image Search</label>
                        <input
                            type="text"
                            id="search-bar-field"
                            name="search-bar"
                            value={this.state.term}
                            onChange={this.onInputChange}></input>
                    </div>
                </form>
            </div>
        );    
    }
}
SearchBar.defaultProps = { className:"" };

export default SearchBar;